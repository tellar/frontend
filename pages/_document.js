import Document, { Head, Main, NextScript } from 'next/document'
import { jsx, css, Global } from '@emotion/core'
import normalize from 'emotion-normalize'

const globalStyles = css`
  ${normalize};

  html {
    box-sizing: border-box;
  }

  *,
  *:before,
  *:after {
    box-sizing: inherit;
  }

  body {
    font-family: 'PT Sans';
    color: #3c3c3c;
  }

  h1 {
    color: tomato;
    text-align: center;
  }
`

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <html>
        <Head>
          <Global styles={globalStyles} />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
