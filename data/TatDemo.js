export const lessonTitle = 'Приветствие / Исәнләшү'
export const lessonData = [
  {
    task: 'Переведите',
    content: {
      text: 'Сәлам',
    },
    action: {
      variants: [
        { id: 0, text: 'Привет' },
        { id: 1, text: 'Пока' },
        { id: 2, text: 'Солома' },
      ],
      answer: [0],
      hint: 'Привет',
    },
  },
  {
    task: 'Переведите',
    content: {
      text: 'Хәлләр ничек?',
    },
    action: {
      variants: [
        { id: 0, text: 'Как дела?' },
        { id: 1, text: 'Как тебя зовут?' },
        { id: 2, text: 'Ты поел?' },
      ],
      answer: [0],
      hint: 'Как дела?',
    },
  },
  {
    task: 'Переведите',
    content: {
      text: 'Яхшы',
    },
    action: {
      variants: [
        { id: 0, text: 'Хорошо' },
        { id: 1, text: 'Плохо' },
        { id: 2, text: 'Нормас' },
      ],
      answer: [0],
      hint: 'Хорошо',
    },
  },
  {
    task: 'Переведите',
    content: {
      text: 'Сау бул',
    },
    action: {
      variants: [
        { id: 0, text: 'Пока' },
        { id: 1, text: 'Привет' },
        { id: 2, text: 'Булка' },
      ],
      answer: [0],
      hint: 'Пока',
    },
  },
  {
    task: 'Переведите',
    content: {
      text: 'Рәхмәт',
    },
    action: {
      variants: [
        { id: 0, text: 'Спасибо' },
        { id: 1, text: 'Привет' },
        { id: 2, text: 'Да' },
      ],
      answer: [0],
      hint: 'Спасибо',
    },
  },
  {
    task: 'Переведите',
    content: {
      text: 'Как дела?',
    },
    action: {
      variants: [
        { id: 0, text: 'Хәлләр' },
        { id: 1, text: 'ничек' },
        { id: 2, text: 'рәхмәт' },
        { id: 3, text: 'Сәлам' },
        { id: 4, text: 'яхшы' },
        { id: 5, text: 'Сау бул' },
      ],
      answer: [0, 1],
      hint: 'Хәлләр ничек?',
    },
  },
]
