import { types } from './actions'
import { STARTED } from 'constants'

const initialState = {
  taskCounter: 0,
  progress: 0,
  answer: [],
  variants: [],
  status: STARTED,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.SET_TASK_COUNTER:
      return { ...state, taskCounter: action.payload.count }
    case types.SET_PROGRESS:
      return { ...state, progress: action.payload.progress }
    case types.SET_ANSWER:
      return { ...state, answer: action.payload.answer }
    case types.SET_VARIANTS:
      return { ...state, variants: action.payload.variants }
    case types.SET_STATUS:
      return { ...state, status: action.payload.status }
    default:
      return state
  }
}
