import shuffle from '../../utils/shuffle'
import { STARTED, PASSED, FAILED, SKIPED } from 'constants'

export const types = {
  SET_TASK_COUNTER: 'currentLesson/SET_TASK_COUNTER',
  SET_PROGRESS: 'currentLesson/SET_PROGRESS',
  SET_ANSWER: 'currentLesson/SET_ANSWER',
  SET_VARIANTS: 'currentLesson/SET_VARIANTS',
  SET_STATUS: 'currentLesson/SET_STATUS',
}

const setTaskCounter = count => {
  return {
    type: types.SET_TASK_COUNTER,
    payload: { count },
  }
}

const setProgress = progress => {
  return {
    type: types.SET_PROGRESS,
    payload: { progress },
  }
}

const setAnswer = answer => {
  return {
    type: types.SET_ANSWER,
    payload: { answer },
  }
}

const setVariants = variants => {
  return {
    type: types.SET_VARIANTS,
    payload: { variants },
  }
}

export const setStatus = status => {
  return {
    type: types.SET_STATUS,
    payload: { status },
  }
}

export const addVariantToAnswer = id => {
  return (dispatch, getState) => {
    const {
      currentLesson: { answer, variants },
    } = getState()

    const nextAnswer = answer.concat([
      variants.find(variant => variant.id === id),
    ])
    const nextVariants = variants.map(variant => {
      if (variant.id === id) {
        return { ...variant, chosen: true }
      }
      return variant
    })

    dispatch(setAnswer(nextAnswer))
    dispatch(setVariants(nextVariants))

    return Promise.resolve()
  }
}

export const removeVariantFromAnswer = id => {
  return (dispatch, getState) => {
    const {
      currentLesson: { answer, variants },
    } = getState()

    const nextAnswer = answer.filter(word => word.id !== id)
    const nextVariants = variants.map(variant => {
      if (variant.id === id) {
        return { ...variant, chosen: false }
      }
      return variant
    })

    dispatch(setAnswer(nextAnswer))
    dispatch(setVariants(nextVariants))

    return Promise.resolve()
  }
}

export const setPassed = () => {
  return (dispatch, getState) => {
    const {
      currentLesson: { progress },
      currentCourse: { tasks },
    } = getState()

    const nextProgress = progress + 1.0 / tasks.length

    dispatch(setStatus(PASSED))
    dispatch(setProgress(nextProgress))

    return Promise.resolve()
  }
}

export const setFailed = () => {
  return (dispatch, getState) => {
    const {
      currentLesson: { progress },
      currentCourse: { tasks },
    } = getState()

    let nextProgress = progress - 1.0 / tasks.length
    if (nextProgress < 0) {
      nextProgress = 0
    }

    dispatch(setStatus(FAILED))
    dispatch(setProgress(nextProgress))

    return Promise.resolve()
  }
}

const startTask = ({ status, progress, taskCounter, answer, variants }) => {
  return (dispatch, getState) => {
    dispatch(setStatus(status))
    dispatch(setProgress(progress))
    dispatch(setTaskCounter(taskCounter))
    dispatch(setAnswer(answer))
    dispatch(setVariants(variants))

    return Promise.resolve()
  }
}

export const startNextTask = () => {
  return (dispatch, getState) => {
    const {
      currentLesson: { progress, taskCounter },
      currentCourse: { tasks },
    } = getState()

    let nextTaskCounter = taskCounter + 1
    let nextProgress = progress

    if (nextTaskCounter >= tasks.length) {
      nextTaskCounter = 0
    }

    if (nextProgress > 0.999) {
      nextProgress = 0
    }

    const variants = shuffle(tasks[nextTaskCounter].action.variants)

    return dispatch(
      startTask({
        status: STARTED,
        progress: nextProgress,
        taskCounter: nextTaskCounter,
        answer: [],
        variants,
      })
    )
  }
}

export const startFirstTask = () => {
  return (dispatch, getState) => {
    const {
      currentCourse: { tasks },
    } = getState()
    const variants = shuffle(tasks[0].action.variants)

    return dispatch(
      startTask({
        status: STARTED,
        progress: 0,
        taskCounter: 0,
        answer: [],
        variants,
      })
    )
  }
}
