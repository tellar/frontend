import { types } from './actions'
import { lessonTitle, lessonData } from 'data/TatDemo'

// Hack for init data
const initialState = {
  tasks: [...lessonData],
  lessonTitle,
}

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
}
