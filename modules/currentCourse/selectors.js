import { createSelector } from 'reselect'
import { getTaskCounter } from 'modules/currentLesson/selectors'

export const getFirstTask = state => state.course.tasks[0]

export const getCurrentTask = createSelector(
  getTaskCounter,
  state => state.currentCourse,
  (taskCounter, currentCourse) => currentCourse.tasks[taskCounter]
)
