import { combineReducers } from 'redux'
import currentCourse from './currentCourse'
import currentLesson from './currentLesson'
import login from './login'

export default combineReducers({ currentCourse, currentLesson, login })
