export const types = {
  UPDATE_LOGIN: 'login/UPDATE_LOGIN',
  UPDATE_PASSWORD: 'login/UPDATE_PASSWORD',
  SUBMIT: 'login/SUBMIT',
}

export const updateLogin = login => ({
  type: types.UPDATE_LOGIN,
  payload: { login },
})

export const updatePassword = password => ({
  type: types.UPDATE_PASSWORD,
  payload: { password },
})

export const submit = () => ({
  type: types.SUBMIT,
})
