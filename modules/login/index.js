import { types } from './actions'

const initialState = {
  login: '',
  password: '',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.UPDATE_LOGIN:
      return { ...state, login: action.payload.login }
    case types.UPDATE_PASSWORD:
      return { ...state, password: action.payload.password }
    default:
      return state
  }
}
