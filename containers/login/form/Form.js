import React, { Component } from 'react'
import { jsx, css } from '@emotion/core'
import Field from './field'

const formStyles = css`
  width: 600px;
  margin: auto;
`

export default ({ login, password, updateLogin, updatePassword, submit }) => (
  <form css={formStyles} onSubmit={submit}>
    <Field
      type="text"
      name="login"
      label="Login"
      value={login}
      onChange={updateLogin}
    />
    <Field
      type="password"
      name="password"
      label="Password"
      value={password}
      onChange={updatePassword}
    />
    <Field type="submit" value="Login" />
  </form>
)
