import React, { Component } from 'react'
import { jsx, css } from '@emotion/core'
import { throttle } from 'throttle-debounce'

const fieldStyles = css`
  display: block;
  width: 200px;
  margin: 0 auto 20px;
`

const labelStyles = css`
  display: block;
  padding-bottom: 5px;
`

const inputStyles = css`
  width: 100%;
  background: white;
  padding: 7px 7px;
`

const submitStyle = css`
  cursor: pointer;
  display: block;
  margin: auto;
  color: white;
  background: green;
  border-radius: 5px;
  border: none;
  padding: 7px 20px;
`

export default ({ label, type, name, value, onChange }) => (
  <label css={fieldStyles}>
    <span css={labelStyles}>{label}</span>
    <input
      type={type}
      name={name}
      value={value}
      onChange={event => throttle(300, onChange(event.target.value))}
      css={type === 'submit' ? submitStyle : inputStyles}
    />
  </label>
)
