import { connect } from 'react-redux'
import { updateLogin, updatePassword, submit } from 'modules/login/actions'
import Form from './Form'

const mapStateToProps = state => ({
  login: state.login.login,
  password: state.login.password,
})

export default connect(
  mapStateToProps,
  { updateLogin, updatePassword, submit }
)(Form)
