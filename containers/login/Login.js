import React, { Component } from 'react'
import Form from './form'

export default props => (
  <div>
    <h1>Login form</h1>
    <Form />
  </div>
)
