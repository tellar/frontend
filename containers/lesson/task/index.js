import { connect } from 'react-redux'
import {
  addVariantToAnswer,
  removeVariantFromAnswer,
} from 'modules/currentLesson/actions'
import { getCurrentTask } from 'modules/currentCourse/selectors'
import Task from './Task'

const mapStateToProps = state => ({
  answer: state.currentLesson.answer,
  variants: state.currentLesson.variants,
  currentTask: getCurrentTask(state),
  lessonTitle: state.currentCourse.lessonTitle,
})

export default connect(
  mapStateToProps,
  { addVariantToAnswer, removeVariantFromAnswer }
)(Task)
