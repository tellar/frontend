import React, { Component } from 'react'
import { jsx, css } from '@emotion/core'

const taskStyle = css`
  width: 600px;
  margin: auto;
`

const titleStyle = css`
  text-align: center;
`
const descriptionStyle = css`
  font-size: 32px;
  font-weight: bold;
`
const contentStyle = css`
  margin-top: 25px;
  font-size: 22px;
`
const answerStyle = css`
  width: 100%;
  height: 50px;
  padding: 5px;
  border-bottom: 2px solid #dadada;
`

const answerWordStyle = css`
  display: inline-block;
  margin: 5px;
  padding: 12px 15px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.2);
  cursor: pointer;
`
const variantsStyle = css`
  margin-top: 25px;
  text-align: center;
`

const variantsVariantStyle = css`
  display: inline-block;
  margin: 5px;
  padding: 12px 15px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.2);
  cursor: pointer;
`

const variantsVariantChosenStyle = css`
  background-color: #e9e9e9;
  box-shadow: none;
  color: transparent;
  cursor: none;
  pointer-events: none;
`

class Task extends Component {
  handleAnswerWordClick = (i, event) => {
    this.props.removeVariantFromAnswer(i)
  }

  handleVariantClick = (i, event) => {
    this.props.addVariantToAnswer(i)
  }

  render() {
    const { answer, variants, currentTask, lessonTitle } = this.props

    return (
      <div css={taskStyle}>
        <h3 css={titleStyle}>{lessonTitle}</h3>
        <div css={descriptionStyle}>{currentTask.task}</div>
        <div css={contentStyle}>{currentTask.content.text}</div>
        <div css={answerStyle}>
          {answer.map((word, i) => (
            <div
              css={answerWordStyle}
              key={`${word.id}-${word.text}-${i}`}
              onClick={this.handleAnswerWordClick.bind(this, word.id)}
            >
              {word.text}
            </div>
          ))}
        </div>
        <div css={variantsStyle}>
          {variants.map((variant, i) => (
            <div
              css={[
                variantsVariantStyle,
                variants[i].chosen && variantsVariantChosenStyle,
              ]}
              key={`${variant.id}-${variant.text}-${i}`}
              onClick={this.handleVariantClick.bind(this, variant.id)}
            >
              {variant.text}
            </div>
          ))}
        </div>
      </div>
    )
  }
}

export default Task
