import React, { Component } from 'react'
import { jsx, css } from '@emotion/core'
import { STARTED, PASSED, FAILED, SKIPED } from 'constants'

const formStyle = css`
  label: form;
  position: relative;
  width: 100%;
  height: 75px;
  margin-top: 50px;
  padding-top: 25px;
`

const formPassedStyle = css`
  label: form--passed;
  background-color: #bff199;
`

const formFailedStyle = css`
  label: form--failed;
  background-color: #ffd3d1;
`

const containerStyle = css`
  label: form__container;
  width: 600px;
  margin: auto;
`

const buttonStyle = css`
  label: form__button;
  display: inline-block;
  width: 150px;
  height: 34px;
  padding: 8px 25px;
  border: 2px solid;
  border-radius: 100px;
  background: green;
  text-align: center;
  line-height: 2;
  font-size: 17px;
  font-weight: bold;
  cursor: pointer;
`

const buttonSkipStyle = css`
  label: form__button-skip;
  border-color: #bfbfbf;
  background-color: transparent;
  color: #777;

  &:hover {
    background-color: rgba(0, 0, 0, 0.05);
    color: #3c3c3c;
  }
`

const buttonSkipDisabledStyle = css`
  label: form__button-skip--disabled;
  border-color: silver;
  background-color: silver;
  color: #e4e4e4;
  cursor: none;
  pointer-events: none;
`

const messageStyle = css`
  label: form__message;
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
`

const buttonSubmitStyle = css`
  label: form__button-submit;
  float: right;
  border-color: #65ab00;
  background-color: #65ab00;
  color: white;

  &:hover {
    border-color: #8ad027;
    background-color: #8ad027;
  }
`

const buttonSubmitDisabledStyle = css`
  label: form__button-submit--disabled;
  border-color: silver;
  background-color: silver;
  color: #e4e4e4;
  cursor: none;
  pointer-events: none;
`

const buttonNextStyle = css`
  label: form__button-next;
  float: right;
  border-color: #65ab00;
  background-color: #65ab00;
  color: white;
`

class Form extends Component {
  handleSkip = event => {
    this.props.setStatus(SKIPED)
  }

  handleSubmit = event => {
    const { answer, taskCounter, currentTask } = this.props
    const keyAnswer = currentTask.action.answer

    let passed = true

    if (keyAnswer.length === answer.length) {
      for (let i = 0; i < keyAnswer.length; i++) {
        if (keyAnswer[i] !== answer[i].id) {
          passed = false
          break
        }
      }
    } else {
      passed = false
    }

    if (passed) {
      this.props.setPassed()
    } else {
      this.props.setFailed()
    }
  }

  handleNext = event => {
    this.props.startNextTask()
  }

  render() {
    const { answer, status, currentTask } = this.props
    const showSuccess = status === PASSED
    const showHint = status === FAILED || status === SKIPED

    return (
      <div
        css={[
          formStyle,
          status === PASSED && formPassedStyle,
          status === FAILED && formFailedStyle,
        ]}
      >
        <div css={containerStyle}>
          {!showHint && !showSuccess ? (
            <div
              css={[
                buttonStyle,
                buttonSkipStyle,
                status !== STARTED && buttonSkipDisabledStyle,
              ]}
              onClick={this.handleSkip}
            >
              Не знаю
            </div>
          ) : null}
          {showHint ? (
            <div css={messageStyle}>
              Правильный ответ:
              <br />
              {currentTask.action.hint}
            </div>
          ) : null}
          {showSuccess ? <div css={messageStyle}>Правильно!</div> : null}
          {status === STARTED ? (
            <div
              css={[
                buttonStyle,
                buttonSubmitStyle,
                answer.length === 0 && buttonSubmitDisabledStyle,
              ]}
              onClick={this.handleSubmit}
            >
              Проверить
            </div>
          ) : (
            <div css={[buttonStyle, buttonNextStyle]} onClick={this.handleNext}>
              Далее
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default Form
