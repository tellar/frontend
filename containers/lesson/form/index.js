import { connect } from 'react-redux'
import {
  setStatus,
  setPassed,
  setFailed,
  startFirstTask,
  startNextTask,
} from 'modules/currentLesson/actions'
import { getCurrentTask } from 'modules/currentCourse/selectors'
import Form from './Form'

const mapStateToProps = state => {
  return {
    taskCounter: state.currentLesson.taskCounter,
    answer: state.currentLesson.answer,
    status: state.currentLesson.status,
    currentTask: getCurrentTask(state),
  }
}

export default connect(
  mapStateToProps,
  {
    setStatus,
    setPassed,
    setFailed,
    startFirstTask,
    startNextTask,
  }
)(Form)
