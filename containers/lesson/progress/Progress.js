import React, { Component } from 'react'
import { jsx, css } from '@emotion/core'

const style = css`
  position: absolute;
  top: 0;
  left: 0;
  border-bottom: 2px solid #37ba00;
`

const Progress = ({ progress }) => (
  <div
    css={css`
      ${style};
      width: ${Math.round(progress * 100)}%;
    `}
  />
)

export default Progress
