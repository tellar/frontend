import { connect } from 'react-redux'
import Progress from './Progress'

const mapStateToProps = state => ({
  progress: state.currentLesson.progress,
})

export default connect(
  mapStateToProps,
  {}
)(Progress)
