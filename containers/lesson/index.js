import { connect } from 'react-redux'
import { startFirstTask } from 'modules/currentLesson/actions'
import Lesson from './Lesson'

export default connect(
  null,
  { startFirstTask }
)(Lesson)
