import React, { Component } from 'react'
import Progress from './progress'
import Task from './task'
import Form from './form'

class Lesson extends Component {
  componentDidMount() {
    this.props.startFirstTask()
  }

  render() {
    return (
      <div className="lesson">
        <Progress />
        <h1>телләр иле</h1>
        <Task />
        <Form />
      </div>
    )
  }
}

export default Lesson
