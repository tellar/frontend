import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import reducers from './modules'

const middleware = [thunk, logger]

export default createStore(reducers, {}, applyMiddleware(...middleware))
